import React from 'react';
import { StyleSheet, Text, View, Button, ImageBackground,TouchableOpacity  } from 'react-native';
import { ColorPicker } from 'react-native-color-picker'
import { Camera} from 'expo-camera';
import { Permissions } from 'expo-permissions';

export default class HomeScreen extends React.Component {
      
  camera = null;

  state = {
      hasCameraPermission: null,
      type : Camera.Constants.Type.back
  };

  async componentDidMount() {
    
    const camera = await Camera.requestPermissionsAsync();
    
    
    console.log("cam status " + camera.status);
    const hasCameraPermission = (camera.status === 'granted');



    this.setState({ hasCameraPermission:hasCameraPermission});
};


  render() {


    const { hasCameraPermission } = this.state;
    const { type } = this.state;

    console.log("perm  " + hasCameraPermission);
    console.log("tu[e]  " + type);

    if (hasCameraPermission === null) {
        return (
          <View style={styles.container}>
            <Text style={styles.text}>Нет доступа к камере! Вы запускаете это приложение на унитазе?</Text>
          </View>
        )
    } else if (hasCameraPermission === false) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>вы отклонили доступ к камере</Text>
        </View>
      )
  }

      return (
        <View style={styles.container}>
   
         <Camera style={{ flex: 1 }} type={type}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              flex: 0.1,
              alignSelf: 'flex-end',
              alignItems: 'center',
            }}
            onPress={() => {
              var itype = 
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back

                  console.log("ttt " + type + ","+ itype);
                  this.setState({type:itype})
              
            }}>
            <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
          </TouchableOpacity>
        </View>
      </Camera>
        </View>
        );
    }
  }
  
  
  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    title: {
      
      fontSize:27,
      color: '#000000'
    },
    text: {
      
      fontSize:27,
      color: '#ffffff'
    }

  });
  
  